properties([
    parameters([
        booleanParam('rollback'), 
        choice(choices: ['deploy', 'build'], name: 'is_deploy'),
        extendedChoice(
            bindings: '', 
            groovyClasspath: '', 
            groovyScript: '''def list = []
                            def files = "/var/lib/jenkins/versionGet.sh".execute()

                            File file = new File("/var/lib/jenkins/versions.txt")
                            file.eachLine { line ->
                            list.add("$line")
                            }

                            return list''', 
            multiSelectDelimiter: ',', 
            name: 'rollback_version', 
            quoteValue: false, 
            saveJSONParameterToFile: false, 
            type: 'PT_SINGLE_SELECT', 
            visibleItemCount: 5
        )
    ])
])
pipeline {
    environment {
        ROLLBACK = "${rollback}"
        IS_DEPLOY = "${is_deploy}"
        ROLLBACK_VERSION = "${rollback_version}"
    }
    agent { label 'harun-agent' }

    stages {
        stage('Prepare') {
            steps {
                script {
                    sh "rm -rf ${WORKSPACE}/build && rm -rf ${WORKSPACE}/node_modules"
                }
            }
        }
        stage('Build') {
            when {
                environment(name: "ROLLBACK", value: "false")
            }
            steps {
                script {
                    nvm('v17.2.0') {
                        sh 'npm install'
                        sh 'npm run build'
                    }
                }
            }
        }
        stage('Artifact') {
            when {
                environment(name: "ROLLBACK", value: "false")
            }
            steps {
                script {
                    dir('build') {
                        sh """
                            zip -r ${env.JOB_NAME}-${env.BUILD_NUMBER}.zip .
                            cp ${env.JOB_NAME}-${env.BUILD_NUMBER}.zip /opt/artifacts/
                        """
                    }
                }
            }
        }
        stage('Deploy') {
            when {
                allOf{
                    environment(name: "ROLLBACK", value: "false")
                    environment(name: "IS_DEPLOY", value: "deploy")
                }
            }
            steps {
                script {
                    sh """
                        rm -rf /usr/share/nginx/html/*
                        unzip /opt/artifacts/${env.JOB_NAME}-${env.BUILD_NUMBER}.zip -d /usr/share/nginx/html/
                        chmod -R 774 /usr/share/nginx/html
                    """
                }
            }
        }
        stage('RollBack') {
            when {
                environment(name: "ROLLBACK", value: "true")
            }
            steps {
                script {
                    sh """
                        rm -rf /usr/share/nginx/html/*
                        unzip /opt/artifacts/$ROLLBACK_VERSION -d /usr/share/nginx/html/
                        chmod -R 774 /usr/share/nginx/html
                    """
                }
            }
        }
    }
}